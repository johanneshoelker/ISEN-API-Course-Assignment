```plantuml
class database
class httprequest

frame infrastructure{
class PatientRepository
class PatientEntity
}

frame domain{
class Patient
}

frame application{
class PatientResource
class PatientService
}

httprequest --> PatientResource
PatientResource --> PatientService: calls
PatientService -> Patient: uses
PatientService --> PatientRepository: calls
PatientRepository -> PatientEntity: uses
PatientRepository --> database: handles
```

This repos is meant to pass the API course at ISEN Yncrea. It is a clone of the professors repo extended with the following funcitonality:

# Practice Manager

## First US and acceptance tests

As a doctor Thomas  
I want to keep records of my patients  
So that i can access them later
Acceptance criteria / Acceptance tests

#### Scenario: Create a patient  only by user docter

Given I am a doctor Thomas  
When I create a patient John Doe with the social security number 123456789123456    

Then I should see the patient in the list of patients

#### Scenario: Search a patient by social security number

Given I am a doctor Thomas   
And I have a patient John Doe with the social security number 123456789123456    
When I search for the patient with the social security number 123456789123456  
Then I should see the patient John Die in the list of patients  

#### Scenario: Search a patient by name

Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I search for the patient with the name John Doe  
Then I should see the patient John Die in the list of patients  

#### Scenario: Search a patient by surname

Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I search for the patient with the surname Doe  
Then I should see the patient John Die in the list of patients  

##### Scenario: Update a patient

Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I update the patient John Doe with the social security number 123456789123457  Then I should see the patient John Doe in the list of patients  
And his social security number should be 123456789123457  

#### Scenario: Delete a patient

Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I delete the patient John Doe  
Then I should not see the patient John Doe in the list of patients  

#### Scenario: Get a patient by ID

Given I am a doctor Thomas  
And I have a patient John Doe with the ID 1  
When I get the patient with the ID 1  
Then I should see the patient John Doe informations

## Run tests using CLI

```bash
python -m unittest tests.patient.application.test_patient_service 
```

## Launch the application

```bash
 flask run
```

## Running Keycloak

```bash
sudo docker run -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:22.0.5 start-dev
```

in keycloak you have to import the practiceManager-keycloak-export.json and create at least the user doctor. With this user you are able to access the delete function.

## Curl examples

### to create a patient:

```bash
curl -v -X POST -H "Content-Type: application/json" \
  -d '{"first_name": "John", "last_name": "Doe", "date_of_birth": "1980-01-01", "social_security_number": 123456789012345}' \
  http://127.0.0.1:5000/patients
```

### to get the list of all patients

```bash
curl -v -X GET http://127.0.0.1:5000/patients
```

### to get a patient by SSN

```bash
curl -v -X GET http://127.0.0.1:5000/patients\?social_security_number=1234567890
```

Due to the fact that the SSN can appear more often, a list of all mathcing patients is returned.

### to get a patient by first name

```bash
curl -v -X GET http://127.0.0.1:5000/patients\?first_name=John
```

## Additional Notes

The test for the deleting with a valid authorization is not working. 
