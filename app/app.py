from flask import Flask, g, request
from flask_oidc import OpenIDConnect
from jose import jwt

from patient.application.patient_resource import create_patient_routes
from patient.application.patient_service import PatientService
from patient.infrastructure.patient_repository import PatientRepository
import json

def create_app():
    app = Flask(__name__)

    app.config.update({
        'SECRET_KEY': 'SomethingNotEntirelySecret',
        'TESTING': True,
        'DEBUG': True,
        'OIDC_CLIENT_SECRETS': 'client_secrets.json',
        'OIDC_ID_TOKEN_COOKIE_SECURE': False,
        'OIDC_USER_INFO_ENABLED': True,
        'OIDC_OPENID_REALM': 'practiceManager',
        'OIDC_SCOPES': ['openid', 'email', 'profile'],
        'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post'
    })
    oidc = OpenIDConnect(app)

    @app.route('/')
    def hello_world():
        if oidc.user_loggedin:
            return ('Hello, %s, <a href="/private">See private</a> '
                    '<a href="/logout">Log out</a>') % \
                oidc.user_getfield('preferred_username')
        else:
            return 'Welcome anonymous, <a href="/private">Log in</a>'

    @app.route('/private')
    @oidc.require_login
    def hello_me():
        """Example for protected endpoint that extracts private information from the OpenID Connect id_token.
            Uses the accompanied access_token to access a backend service.
        """

        info = oidc.user_getinfo(['preferred_username', 'email', 'sub'])

        username = info.get('preferred_username')
        email = info.get('email')
        user_id = info.get('sub')

        if user_id:
            access_token = oidc.get_access_token()
            print('access_token=<%s>' % access_token)

        return ("""your email is %s and your user_id is %s!
                   <ul>
                     <li><a href="/">Home</a></li>
                     <li><a href="//localhost:8080/realms/practiceManager/account?referrer=flask-app&referrer_uri=http://localhost:5000/private&">Account</a></li>
                    </ul>""" %
                (email, user_id))


    @app.route('/hello', methods=['GET'])
    @oidc.accept_token()
    def hello_api():
        """OAuth 2.0 protected API endpoint accessible via AccessToken"""
        token = request.headers.get('Authorization').split(' ')[1]
        claim = jwt.get_unverified_claims(token)
        preferred_username = claim.get('preferred_username')

        return json.dumps({'hello': 'Welcome %s' % preferred_username})

    @app.route('/logout')
    def logout():
        """Performs local logout by removing the session cookie."""

        oidc.logout()
        return 'Hi, you have been logged out! <a href="/">Return</a>'


    # Configuration settings, routes, middleware, etc. can be added here
    patient_repository = PatientRepository()
    patient_service = PatientService(patient_repository)
    app.config['patient_service'] = patient_service

    create_patient_routes(app, oidc)
    return app

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)
