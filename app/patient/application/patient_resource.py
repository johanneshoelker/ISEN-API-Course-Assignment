from flask import request, jsonify, Blueprint
from flask import current_app as app
from patient.domain.patient import Patient


def create_patient_routes(app, oidc):
    @app.route('/patients', methods=['POST'])
    def create_patient():
        try:
            patient_data = request.get_json()
            patient_service = app.config['patient_service']
            created_patient = patient_service.create_patient(Patient(**patient_data))

            return jsonify(created_patient.to_dict()), 201
        except Exception as e:
            return jsonify({'error': str(e)}), 400

    @app.route('/patients/<int:patient_id>', methods=['POST'])
    def update_patient(patient_id):
        try:
            patient_data = request.get_json()
            patient_service = app.config['patient_service']
            updated_patient = patient_service.update_patient(patient_id, Patient(**patient_data))
            return jsonify(updated_patient.to_dict()), 201
        except Exception as e:
            return jsonify({'error': str(e)}), 400

    @app.route('/patients/<int:patient_id>', methods=['GET'])
    def get_patient(patient_id):
        try:
            patient_service = app.config['patient_service']

            searched_patient = patient_service.get_patient(patient_id)
            return jsonify(searched_patient.to_dict()), 200
        except Exception as e:
            return jsonify({'error': str(e)}), 400

    @app.route('/patients', methods=['GET'])
    def get_patients_list():
        try:
            patient_service = app.config['patient_service']
            # Check if parameters are present in the URL
            parameters = request.args

            if 'social_security_number' in parameters:
                # Call service with parameter
                ssn = parameters['social_security_number']
                result = patient_service.get_patient_by_ssn(ssn)
            elif 'first_name' in parameters:
                # Call service with parameter
                name = parameters['first_name']
                result = patient_service.get_patient_by_first_name(name)
            elif 'last_name' in parameters:
                # Call service with parameter
                name = parameters['last_name']
                result = patient_service.get_patient_by_last_name(name)
            else:
                # Call service without parameter
                result = patient_service.get_patients_list()
            return result, 200
        except Exception as e:
            return jsonify({'error': str(e)}), 400

    @app.route('/patients/<int:patient_id>', methods=['DELETE'])
    @oidc.accept_token()
    def delete_patient(patient_id):


        user_claims = oidc.user_getinfo(['preferred_username', 'email', 'username'])
        allowed_user = "doctor"
        print(user_claims.get('preferred_username'))
        print(allowed_user)
        if user_claims.get('preferred_username') != allowed_user:
            return jsonify({'error': 'Forbidden. Access not allowed for this user.'}), 403

        try:
            patient_service = app.config['patient_service']
            deletion_success = patient_service.delete_patient(patient_id)
            return jsonify({'Patient deleted':str(deletion_success)}), 204
        except Exception as e:
            return jsonify({'error': str(e)}), 400
