from datetime import datetime

from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity


class PatientService:
    def __init__(self, patient_repository):
        self.patient_repository = patient_repository

    def create_patient(self, patient: Patient) -> PatientEntity:
        required_fields = ['first_name', 'last_name', 'date_of_birth', 'social_security_number']

        if any(not getattr(patient, field, None) for field in required_fields):
            raise ValueError('Patient first name is required')

        if patient.date_of_birth != datetime.strptime(patient.date_of_birth, '%Y-%m-%d') \
                .strftime('%Y-%m-%d'):
            raise ValueError('Patient date of birth is invalid')

        created_patient = self.patient_repository.add_patient(patient)
        return created_patient

    def get_patient(self, id) -> PatientEntity:
        searched_patient = self.patient_repository.get_patient(id)
        return searched_patient

    def update_patient(self,patient_id, patient:Patient) -> PatientEntity:
        updated_patient = self.patient_repository.update_patient(patient_id, patient)
        return updated_patient

    # Gets complete database and switches it to an array of patients (so no dictionary anymore)
    def get_patients_list(self):
        db = self.patient_repository.get_database()
        patients_list=[]
        for key, record in db.items():
            patients_list.append(record.to_dict())
        return patients_list

    def get_patient_by_ssn(self, ssn):
        db = self.patient_repository.get_database()
        patients_list_ssn_match=[]
        for key, record in db.items():
            if str(record.social_security_number) == str(ssn):
                patients_list_ssn_match.append(record.to_dict())
        return patients_list_ssn_match

    def get_patient_by_first_name(self, first_name):
        db = self.patient_repository.get_database()
        patients_list_first_name_match=[]
        for key, record in db.items():
            if str(record.first_name) == str(first_name):
                patients_list_first_name_match.append(record.to_dict())
        return patients_list_first_name_match

    def get_patient_by_last_name(self, last_name):
        db = self.patient_repository.get_database()
        patients_list_last_name_match=[]
        for key, record in db.items():
            if str(record.last_name) == str(last_name):
                patients_list_last_name_match.append(record.to_dict())
        return patients_list_last_name_match

    def delete_patient(self, id):
        self.patient_repository.delete_patient(id)
        return True
