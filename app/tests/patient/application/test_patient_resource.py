import unittest
from unittest.mock import Mock
from flask.testing import FlaskClient

from app import create_app
from patient.application.patient_service import PatientService
from patient.infrastructure.patient_entity import PatientEntity
import jwt
from datetime import datetime, timedelta


class TestPatientResource(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['TESTING'] = True
        self.client: FlaskClient = self.app.test_client()

        self.mock_service = Mock(spec=PatientService)
        self.app.config['patient_service'] = self.mock_service

    def test_create_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_service.create_patient.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.post('/patients', json=patient_data)

        self.assertEqual(201, response.status_code)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.create_patient.assert_called_once()

    def test_get_patients_list_endpoint(self):
        # Given:
        patients_list_mock=[
        {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        },
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        ]
        self.mock_service.get_patients_list.return_value = patients_list_mock

        # When:
        patients_list = self.client.get('/patients', json = {}).json
        # Then:
        self.assertEqual(patients_list, patients_list_mock)

    def test_update_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_service.update_patient.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.post('/patients/1', json=patient_data)

        self.assertEqual(201, response.status_code)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.update_patient.assert_called_once()

    def test_get_patient_by_id_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }
        self.mock_service.get_patient.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.get('/patients/1')
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.get_patient.assert_called_once()

    def test_delete_patient_endpoint_no_auth(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_service.delete_patient.return_value = True
        response = self.client.delete('/patients/2')

        self.assertEqual(401, response.status_code)

    # def test_delete_patient_endpoint_with_auth(self):
    #
    #     # Replace 'your_keycloak_secret' with your actual Keycloak client secret
    #     keycloak_secret = 'K48aTrtVQRgii3JWHYFJF3CpwJNKNiyo'
    #
    #     # Replace 'your_keycloak_client_id' with your actual Keycloak client ID
    #     keycloak_client_id = 'patient-backend'
    #
    #     # Replace 'your_keycloak_realm' with your actual Keycloak realm
    #     keycloak_realm = 'practiceManager'
    #
    #     # Replace 'doctor_username' with the username of the doctor
    #     doctor_username = 'doctor'
    #
    #
    #     expiration_time = datetime.utcnow() + timedelta(hours=1)
    #
    #     payload = {
    #         'sub': doctor_username,
    #         'exp': expiration_time,
    #         'iss': f'https://your-keycloak-server/auth/realms/{keycloak_realm}',
    #         'aud': keycloak_client_id,
    #         'iat': datetime.utcnow(),
    #         'nbf': datetime.utcnow(),
    #     }
    #
    #     access_token = jwt.encode(payload, keycloak_secret, algorithm='HS256')
    #
    #
    #     print(access_token)
    #
    #     patient_data = {
    #         'first_name': 'John',
    #         'last_name': 'Doe',
    #         'date_of_birth': '1980-01-01',
    #         'social_security_number': 123456789012345
    #     }
    #
    #     self.mock_service.delete_patient.return_value = True
    #     response = self.client.delete('/patients/2', headers={'Authorization': f'Bearer {access_token}'})
    #
    #     # self.assertEqual(201, response.status_code)
    #     self.assertEqual({'Patient deleted':'True'}, response.json)
    #     self.mock_service.delete_patient.assert_called_once()

    def test_get_patient_by_ssn_endpoint(self):
        # Given:
        patients_list_mock=[
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }
        ]
        self.mock_service.get_patient_by_ssn.return_value = patients_list_mock

        # When:
        patients_list_by_ssn = self.client.get('/patients?social_security_number=1234567890').json
        # Then:
        self.assertEqual(patients_list_by_ssn, patients_list_mock)

    def test_get_patient_by_first_name_endpoint(self):
        # Given:
        patients_list_mock=[
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }
        ]
        self.mock_service.get_patient_by_first_name.return_value = patients_list_mock

        # When:
        patients_list_by_first_name = self.client.get('/patients?first_name=John').json
        # Then:
        self.assertEqual(patients_list_by_first_name, patients_list_mock)

    def test_get_patient_by_last_name_endpoint(self):
        # Given:
        patients_list_mock=[
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }
        ]
        self.mock_service.get_patient_by_last_name.return_value = patients_list_mock

        # When:
        patients_list_by_last_name = self.client.get('/patients?last_name=Doe').json
        # Then:
        self.assertEqual(patients_list_by_last_name, patients_list_mock)
