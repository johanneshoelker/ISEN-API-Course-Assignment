import unittest
import unittest.mock as mock
from patient.application.patient_service import PatientService
from patient.domain.patient import Patient

from patient.infrastructure.patient_entity import PatientEntity
import json

class TestPatientService(unittest.TestCase):
    def setUp(self):
        self.mock_repository = mock.Mock()
        self.patient_service = PatientService(self.mock_repository)

    def test_create_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_repository.add_patient.return_value = {'id': 1, **patient_data}

        new_patient = self.patient_service.create_patient(Patient(**patient_data))

        self.assertIsNotNone(new_patient)
        self.assertEqual(new_patient['id'], 1)
        self.assertEqual(new_patient['first_name'], 'John')
        self.assertEqual(new_patient['last_name'], 'Doe')
        self.assertEqual(new_patient['date_of_birth'], '1980-01-01')
        self.assertEqual(new_patient['social_security_number'], 123456789012345)

        self.mock_repository.add_patient.assert_called_once()

    def create_patient_with_missing_value_raises_value_error(self, missing_field):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        del patient_data[missing_field]

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

    def test_create_patient_with_missing_first_name(self):
        self.create_patient_with_missing_value_raises_value_error('first_name')

    def test_create_patient_with_missing_last_name(self):
        self.create_patient_with_missing_value_raises_value_error('last_name')

    def test_create_patient_with_missing_date_of_birth(self):
        self.create_patient_with_missing_value_raises_value_error('date_of_birth')

    def test_create_patient_with_missing_social_security_number(self):
        self.create_patient_with_missing_value_raises_value_error('social_security_number')

    def test_create_patient_with_invalid_birth_date(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-13-01',
            'social_security_number': '123456789012345'
        }

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

        self.mock_repository.add_patient.assert_not_called()

    def test_get_patient_by_id(self):
        patient_id =1
        patient_data = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-12-01',
            'social_security_number': '123456789012345'
        }
        self.mock_repository.get_patient.return_value = PatientEntity(**patient_data)

        searched_patient = self.patient_service.get_patient(patient_id)
        self.mock_repository.get_patient.assert_called_once_with(patient_id)
        self.assertEqual(searched_patient.id, patient_id)
        self.assertEqual(searched_patient.first_name, 'John')
        self.assertEqual(searched_patient.last_name, 'Doe')
        self.assertEqual(searched_patient.date_of_birth, '1980-12-01')
        self.assertEqual(searched_patient.social_security_number, '123456789012345')

    def test_update_patient(self):
        # Given:
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }
        patient_id=1
        self.mock_repository.update_patient.return_value = {'id': patient_id, **patient_data}

        # When:
        updated_patient = self.patient_service.update_patient(patient_id, Patient(**patient_data))

        # Then:
        self.assertIsNotNone(updated_patient)
        self.assertEqual(updated_patient['id'], 1)
        self.assertEqual(updated_patient['first_name'], 'John')
        self.assertEqual(updated_patient['last_name'], 'Doe')
        self.assertEqual(updated_patient['date_of_birth'], '1980-01-01')
        self.assertEqual(updated_patient['social_security_number'], 123456789012345)

        self.mock_repository.update_patient.assert_called_once()

    def test_get_patients_list(self):
        # Given:
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        patient_entity1 = PatientEntity(**{'id':1, **patient_data})
        patient_entity2 = PatientEntity(**{'id':2, **patient_data})
        patients_list_mock = {1:patient_entity1, 2:patient_entity2}

        patients_list_result=[
        {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        },
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        ]
        self.mock_repository.get_database.return_value = patients_list_mock

        # When:
        retrieved_patients_list = self.patient_service.get_patients_list()

        # Then:
        self.assertEqual(retrieved_patients_list, patients_list_result)

    def test_get_patient_by_ssn(self):
        # Given:
        ssn = '1234567890'
        patient_data1 = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient_data2 = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': ssn
        }
        patient_entity1 = PatientEntity(**{'id':1, **patient_data1})
        patient_entity2 = PatientEntity(**{'id':2, **patient_data2})
        patients_list_mock = {1:patient_entity1, 2:patient_entity2}

        patients_list_result=[
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }]
        self.mock_repository.get_database.return_value = patients_list_mock

        # When:
        retrieved_patients_list_ssn_match = self.patient_service.get_patient_by_ssn(ssn)

        # Then:
        self.assertEqual(retrieved_patients_list_ssn_match, patients_list_result)

    def test_get_patient_by_first_name(self):
        # Given:
        first_name = 'John'
        patient_data1 = {
            'first_name': 'Joey',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient_data2 = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }
        patient_entity1 = PatientEntity(**{'id':1, **patient_data1})
        patient_entity2 = PatientEntity(**{'id':2, **patient_data2})
        patients_list_mock = {1:patient_entity1, 2:patient_entity2}

        patients_list_result=[
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }]
        self.mock_repository.get_database.return_value = patients_list_mock

        # When:
        retrieved_patients_list_first_name_match = self.patient_service.get_patient_by_first_name(first_name)

        # Then:
        self.assertEqual(retrieved_patients_list_first_name_match, patients_list_result)

    def test_get_patient_by_last_name(self):
        # Given:
        last_name = 'Doe'
        patient_data1 = {
            'first_name': 'Joey',
            'last_name': 'Doey',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient_data2 = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }
        patient_entity1 = PatientEntity(**{'id':1, **patient_data1})
        patient_entity2 = PatientEntity(**{'id':2, **patient_data2})
        patients_list_mock = {1:patient_entity1, 2:patient_entity2}

        patients_list_result=[
        {
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }]
        self.mock_repository.get_database.return_value = patients_list_mock

        # When:
        retrieved_patients_list_last_name_match = self.patient_service.get_patient_by_last_name(last_name)

        # Then:
        self.assertEqual(retrieved_patients_list_last_name_match, patients_list_result)

if __name__ == '__main__':
    unittest.main()
