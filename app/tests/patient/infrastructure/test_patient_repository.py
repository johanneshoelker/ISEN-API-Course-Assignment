import unittest
from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity
from patient.infrastructure.patient_repository import PatientRepository


class TestPatientRepository(unittest.TestCase):
    def setUp(self):
        self.repository = PatientRepository()

    def test_add_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        self.assertEqual(self.repository.get_patient(new_patient.id), PatientEntity(**{'id': 1, **patient_data}))

    def test_get_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        retrieved_patient = self.repository.get_patient(new_patient.id)

        self.assertEqual(retrieved_patient, PatientEntity(**{'id': 1, **patient_data}))

    def test_update_patient(self):
        patient_id=1
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        updated_patient = self.repository.update_patient(patient_id, patient)
        self.assertEqual(self.repository.get_patient(patient_id), PatientEntity(**{'id': patient_id, **patient_data}))

    def test_get_database(self):
        # Given:
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        patient_entity1 = PatientEntity(**{'id':1, **patient_data})
        patient_entity2 = PatientEntity(**{'id':2, **patient_data})
        patient_entity3 = PatientEntity(**{'id':3, **patient_data})
        patients_list = {1:patient_entity1, 2:patient_entity2, 3:patient_entity3}

        # When:
        new_patient1 = self.repository.add_patient(patient)
        new_patient2 = self.repository.add_patient(patient)
        new_patient3 = self.repository.add_patient(patient)

        retrieved_patients_list = self.repository.get_database()

        # Then:
        self.assertEqual(retrieved_patients_list, patients_list)


    def test_delete_patient(self):
        # Given
        patient_id = 2
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)

        # When:
        new_patient1 = self.repository.add_patient(patient)
        new_patient2 = self.repository.add_patient(patient)
        new_patient3 = self.repository.add_patient(patient)

        deletion_success = self.repository.delete_patient(patient_id)

        deleted_patient = self.repository.get_patient(patient_id)
        # Then:
        self.assertEqual(deletion_success, True)
        self.assertEqual(deleted_patient, None)
